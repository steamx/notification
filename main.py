import json
import smtplib
import ssl

from fastapi import FastAPI
import paho.mqtt.client as mqtt

port = 1025
smtp_server = "smtp-server"
sender_email = "my@gmail.com"
receiver_email = "your@gmail.com"

context = ssl.create_default_context()
server = smtplib.SMTP(smtp_server, port)

mqtt_client = mqtt.Client("notification_service_mqtt")
mqtt_client.connect("mosquitto")


def on_message(client, userdata, message):
    print("message received ", str(message.payload.decode("utf-8")))
    games = json.loads(str(message.payload.decode("utf-8")))

    gamesAsString = '\n'.join(list(map(lambda x: x['name'], games)))

    server.sendmail("notification@steamx.com", "konkord320@gmail.com", gamesAsString)


mqtt_client.on_message = on_message
mqtt_client.loop_start()
mqtt_client.subscribe("games/random")


app = FastAPI()

@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.get("/hello/{name}")
async def say_hello(name: str):
    return {"message": f"Hello {name}"}
