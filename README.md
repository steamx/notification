# Notification

## About
The small service written in python for notification domain which is the only component that should handle any kind of notifications in steamx application. For now it listens on `games/random` MQTT topic to notify interested users about 10 random games every 3 minutes. 

## How to run
`docker build -t notification .`
`docker run -p 8080:80 notification`

To run whole steamx application check `Infrastructure` repository